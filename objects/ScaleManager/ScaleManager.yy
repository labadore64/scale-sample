{
    "id": "28fec97c-bac1-4868-aeb7-ba1bd005bc07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ScaleManager",
    "eventList": [
        {
            "id": "3034de96-c671-466f-a594-9595c228c2bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        },
        {
            "id": "777658ee-d383-4b07-b70a-86508d909f5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        },
        {
            "id": "feeeba27-f786-4746-a6ef-30d2b40f2d32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        },
        {
            "id": "ace0cc03-e1d8-40da-9e83-e926576e3260",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        },
        {
            "id": "7e6aba9e-353c-4ddc-ae0e-20dcb9d95247",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "28fec97c-bac1-4868-aeb7-ba1bd005bc07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a282401e-ad79-41ff-b801-f592d28b8c7d",
    "visible": true
}