// disables auto-scaling
application_surface_draw_enable(false);

// If this is true the scaler was updated this frame.
// Use this to redraw your surfaces!
global.scale_updated = false;

// This is the initial aspect ratio.
global.base_aspect_ratio = 800/600;

// These are the initial dimensions.
global.base_width = 800;
global.base_height = 600;

// These are the current window size dimensions.
global.window_width = window_get_width();
global.window_height = window_get_height();
	
// this is the current aspect ratio.
global.aspect_ratio = global.window_width/global.window_height;

// Do the scale routine!		
scaler_DoScale();

// These are used for drawing the background scaled.
base_xscale = 1;
base_yscale = 1;
