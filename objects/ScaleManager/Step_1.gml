// Reset the updated flag.
global.scale_updated = false;

// If either window dimension has changed this frame
if ( window_get_width() != global.window_width ||
         window_get_height() != global.window_height )
    {
        // if the window frame size is not 0 for either dimension
        // (You want to do this because quitting the game can cause issues)
		if(window_get_width() != 0 && window_get_height() != 0){

            // Update the window variables.
	        global.window_width = window_get_width();
	        global.window_height = window_get_height();
   
            // Resize the application surface.
	        surface_resize(application_surface, global.window_width, global.window_height)    
		
            // Reset the aspect ratio.
			global.aspect_ratio = global.window_width/global.window_height;
		
            // Update the scaler variables.
			scaler_DoScale();
		    
            // Flag the scaler as updated.
			global.scale_updated = true;
		}
    }
