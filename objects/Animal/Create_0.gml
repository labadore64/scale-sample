// Manages the coil function
coil = 0;
coil_rate = .25
coil_max = 10;

// Manages the release function
release = 0;
release_counter = 0;

// Base scaling variables
base_xscale = 1;
base_yscale = 1;
base_x = x;
base_y = y;

// Determines the click area
x_click_size = 50
y_click_size = 200
