// Checks if the mouse is in the right place.
// Notice that it uses a bottom middle center point.
if(mouse_x >= base_x-x_click_size &&
	mouse_x <= base_x+x_click_size &&
	mouse_y >= base_y-y_click_size &&
	mouse_y <= base_y){
    // sets the coil.
	coil+= coil_rate;
	if(coil > coil_max){
		coil = coil_max;	
	}
    
    // Updates the scale value.
	base_yscale = 1 - coil*.05

    // prevents it from releasing while coiling.
	release = 0;
}
