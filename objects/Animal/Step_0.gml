// If releasing
if(release > 0){
    // reduce the release factor.
	release-=(coil_rate);

    // increase the release counter (for the sine wave)
	release_counter++
	
    // Generate the bounce effect
	base_yscale = 1 - cos(release_counter*.36)*.05*release
	
    // if the release is completed, reset.
	if(release < 1){
		coil = 0;

		release = 0;
		release_counter = 0;
		base_yscale = 1
	}
}

