{
    "id": "12ca1e70-89ab-4280-81da-660ecb9c0904",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Animal",
    "eventList": [
        {
            "id": "8e850e08-ab62-42f5-967d-f5f86ad11951",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "12ca1e70-89ab-4280-81da-660ecb9c0904"
        },
        {
            "id": "58b6918b-9f7b-4253-a649-6e8efc54a104",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12ca1e70-89ab-4280-81da-660ecb9c0904"
        },
        {
            "id": "8df3692d-774c-4aac-9992-7d9e613419cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "12ca1e70-89ab-4280-81da-660ecb9c0904"
        },
        {
            "id": "ab323e63-6da9-47f4-850f-a4c31f8ca07a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "12ca1e70-89ab-4280-81da-660ecb9c0904"
        },
        {
            "id": "889a836d-cdeb-42bd-8a60-a9ebfe66bf2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "12ca1e70-89ab-4280-81da-660ecb9c0904"
        },
        {
            "id": "a5c65390-90f4-4c6f-9efb-32e318450e05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "12ca1e70-89ab-4280-81da-660ecb9c0904"
        },
        {
            "id": "7565c47b-9721-42b1-9dcc-7d58c583688d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "12ca1e70-89ab-4280-81da-660ecb9c0904"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a4a88044-1518-41aa-9935-a4e48499ce63",
    "visible": true
}