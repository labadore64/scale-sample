if (global.aspect_ratio > global.base_aspect_ratio) // window's fatter than display, vertical letterbox
{
	global.letterbox_h = global.window_height;
	global.scale_factor = global.window_height/global.base_height;
	global.letterbox_w = global.base_width*global.scale_factor;
	global.display_x = (global.window_width) * 0.5 - global.letterbox_w* 0.5 ;
	global.display_y = 0;
}
else  // window's skinnier than display, horizontal letterbox
{
	global.letterbox_w = global.window_width;
	global.scale_factor = global.window_width/global.base_width;
	global.letterbox_h = global.base_height*global.scale_factor;
	global.display_x = 0;
	global.display_y = (global.window_height) * 0.5 - global.letterbox_h* 0.5 ;
}

// update the scale manager's background

image_xscale = global.scale_factor
image_yscale = global.scale_factor
x = global.display_x
y = global.display_y;