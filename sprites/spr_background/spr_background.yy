{
    "id": "a282401e-ad79-41ff-b801-f592d28b8c7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfbe3167-995d-4c97-8afd-e0d8c7a62af2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a282401e-ad79-41ff-b801-f592d28b8c7d",
            "compositeImage": {
                "id": "e207a20b-ccd1-4b68-a457-f21ca562f786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfbe3167-995d-4c97-8afd-e0d8c7a62af2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5254d2aa-0961-4bd8-bf20-78d85ad74eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfbe3167-995d-4c97-8afd-e0d8c7a62af2",
                    "LayerId": "29688e58-0907-4b62-a0ed-53a9d514cb4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "29688e58-0907-4b62-a0ed-53a9d514cb4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a282401e-ad79-41ff-b801-f592d28b8c7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}