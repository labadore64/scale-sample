{
    "id": "a4a88044-1518-41aa-9935-a4e48499ce63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pierre",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 96,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "267ccda5-a187-407e-9069-f91857294c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4a88044-1518-41aa-9935-a4e48499ce63",
            "compositeImage": {
                "id": "e4e64308-93f1-4c02-b316-99ec92ceab48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "267ccda5-a187-407e-9069-f91857294c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "146ea050-b3b7-4c38-bc0b-45c6f37159f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "267ccda5-a187-407e-9069-f91857294c82",
                    "LayerId": "c1e9dde6-a974-41ac-b5e8-b6493a42875e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 176,
    "layers": [
        {
            "id": "c1e9dde6-a974-41ac-b5e8-b6493a42875e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4a88044-1518-41aa-9935-a4e48499ce63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 97,
    "xorig": 60,
    "yorig": 176
}