{
    "id": "476026f0-0e06-4a91-a56e-5ea41759af62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_leon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 0,
    "bbox_right": 114,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c88c6722-db31-47de-89fe-bd04d734fcd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "476026f0-0e06-4a91-a56e-5ea41759af62",
            "compositeImage": {
                "id": "6582b54d-5d28-4243-b32d-8cac933edf5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c88c6722-db31-47de-89fe-bd04d734fcd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f3f9365-d983-4845-838b-4fd12f8a7f9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c88c6722-db31-47de-89fe-bd04d734fcd1",
                    "LayerId": "d07e1e9d-ba95-480e-8c42-eac82cc6e660"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 153,
    "layers": [
        {
            "id": "d07e1e9d-ba95-480e-8c42-eac82cc6e660",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "476026f0-0e06-4a91-a56e-5ea41759af62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 52,
    "yorig": 152
}